# Study Python

<https://www.runoob.com/django/>

## install Python3 on MacOS

```bash
brew search python3
brew install python3
xcode-select --install
brew upgrade python
```

## Install dependencies

(Python=3.7.0, Django==2.2.6, pytz==2019.3, sqlparse==0.3.0)

```bash
pip3 install -r requirements.txt
pip3 list
```

## Run ui server

```bash
python3 manage.py runserver localhost:8000
open http://localhost:8000/admin/
open http://localhost:8000/view/
```

## Run shell server

```bash
python3 manage.py createsuperuser
python3 manage.py shell
```

## Change superuser's password

```python
from django.contrib.auth.models import User
user = User.objects.get(pk=1)
user = User.objects.get(username='admin')
user.set_password('new password')
user.save()
```

![screenshot](./screenshot.png)
