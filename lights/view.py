# -*- encoding: utf-8 -*-
"""
@File    : view.py
@Time    : 2019/10/28 4:11 下午
@Author  : david wang
@Email   : 3063737@qq.com
@Software: PyCharm
"""
from django.http import HttpResponse

def index(request):
    return HttpResponse("Hello world ! ")

from django.shortcuts import render

def hello(request):
    context = {}
    context['hello'] = 'Hello World!'
    return render(request, 'index.html', context)

def about(request):
    context = {}
    return render(request, 'about.html', context)

def contact(request):
    context = {}
    return render(request, 'contact.html', context)

def services(request):
    context = {}
    return render(request, 'services.html', context)
